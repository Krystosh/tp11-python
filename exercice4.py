week_end_mai = {"Pierre" : [12,70,10], "Paul" : [100], "Marie" : [15], "Anna" : []}
week_end_juin = {"Jul" : [145,56,45.24], "MAES" : [56.55,78,15], "GMK" : [1500000,470000],"Lorenzo" : [15,15,15,15,15]}
def cumul(week) :
    res = dict()
    for (nom,depense) in week.items():
        if depense == [] : 
            res[nom] = 0
        for val in depense :
            if nom not in res.keys():
                res[nom] = val
            else :
                res[nom]+=val
    return res
#print(cumul(week_end_mai))

def affiche_bilan_financier(week) :
    total = 0
    cpt = 0
    depense_chaqu = cumul(week)
    for nb in depense_chaqu.values() :
        total+=nb
        cpt += 1
    if cpt != 0 :
        tot_per_pers = total/cpt
    for (nom,depense) in depense_chaqu.items() :
        if depense > tot_per_pers :
            print(nom,"Doit recevoir",depense-tot_per_pers,"euros")
        elif depense < tot_per_pers :
            print(nom,"Doit verser",tot_per_pers-depense,"euros")  
affiche_bilan_financier(week_end_mai)
    
